""" Django view module
"""

import json
import time
import math
from pygeohash.geohash import encode 
from pygeohash.geohash import decode
from django.http import JsonResponse, HttpResponseBadRequest
from django.conf import settings
from rest_framework.views import APIView
from api.location.gps import distance
from api.firebase_auth.authentication import TokenAuthentication
from api.firebase_auth.permissions import FirebasePermissions
from api.spam.classifier import classify_text
from api.spam.views import get_spam_report_data
from api.notifications.dispatch import notify_incident
__base32 = '0123456789bcdefghjkmnpqrstuvwxyz'

_PRECISION = {
    0: 20000000,
    1: 5003530,
    2: 625441,
    3: 123264,
    4: 19545,
    5: 3803,
    6: 610,
    7: 118,
    8: 19,
    9: 3.71,
    10: 0.6,
}
def geohash_approximate_distance(geohash_1, geohash_2, check_validity=False):
    """
    Returns the approximate great-circle distance between two geohashes in meters.
    :param geohash_1:
    :param geohash_2:
    :return:
    """

    if check_validity:
        if len([x for x in geohash_1 if x in __base32]) != len(geohash_1):
            raise ValueError('Geohash 1: %s is not a valid geohash' % (geohash_1, ))

        if len([x for x in geohash_2 if x in __base32]) != len(geohash_2):
            raise ValueError('Geohash 2: %s is not a valid geohash' % (geohash_2, ))

    # normalize the geohashes to the length of the shortest
    len_1 = len(geohash_1)
    len_2 = len(geohash_2)
    if len_1 > len_2:
        geohash_1 = geohash_1[:len_2]
    elif len_2 > len_1:
        geohash_2 = geohash_2[:len_1]

    # find how many leading characters are matching
    matching = 0
    for g1, g2 in zip(geohash_1, geohash_2):
        if g1 == g2:
            matching += 1
        else:
            break

    # we only have precision metrics up to 10 characters
    if matching > 10:
        matching = 10

    return _PRECISION[matching]



DB = settings.FIREBASE.database()

class EventView(APIView):
    """ API view class for events
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (FirebasePermissions,)

    def get(self, request):
        """ Returns data for a event for a given location

        GET request parameters:
            [REQUIRED]
            id: firebase event id
        Arguments:
            request {[type]} -- [ Contains the django request object]
        Returns:
            [HttpResponseBadRequest] -- [If  event id is not given]
            [JsonResponse] -- [Containing the event data]
        """
        query = request.GET.get('id', '')
        if query == '':
            return HttpResponseBadRequest("Bad request: No Id specified")

        data = DB.child('incidents').child(query).get().val()
        for key in data['reportedBy']:
            if data['reportedBy'][key]['anonymous']:
                data['reportedBy'][key] = {
                    'displayName': "Anonymous",
                    'photoURL': 'https://crowdalert.herokuapp.com/static/images/meerkat.svg',
                }
            else:
                user_id = data['reportedBy'][key]['userId']
                udata = DB.child('users/' + user_id).get().val()
                data['reportedBy'][key] = {
                    'displayName': udata['displayName'],
                    'photoURL': udata['photoURL'],
                }
        data['spam'] = get_spam_report_data(query)
        return JsonResponse(data, safe=False)

    def post(self, request):
        """Post event to firebase DB.

        Potential required features:
            Custom validation
            Location validation
            Spam classification
        """
        event_data = json.loads(request.body.decode()).get('eventData', '')
        if event_data == '':
            return HttpResponseBadRequest("Bad request")
        decoded_json = json.loads(event_data)
        uid = str(request.user)

        latitude = decoded_json['location']['coords']['latitude']
        longitude = decoded_json['location']['coords']['longitude']
        geohash = encode(latitude,longitude) 
        # precision to be added in encode 
        incident_data = {
            "category": decoded_json['category'],
            "datetime": int(time.time()*1000),
            "description": decoded_json['description'],
            "local_assistance": decoded_json['local_assistance'],
            "location": {
                "coords": {
                    "latitude": latitude,
                    "longitude": longitude,
                },
                "geohash": geohash ,
            },
            "public": {
                "share": decoded_json['public']['share'],
                "view":  decoded_json['public']['view'],
            },
            "reportedBy": {
                "original": {
                    "userId": uid,
                    "anonymous": decoded_json['anonymous'],
                },
            },
            "title": decoded_json['title']
        }

        data = DB.child('incidents').push(incident_data)

        key = data['name']
        DB.child('incidentReports/' + uid).push({
            "incidentId": key,
        })
        # Add the comments section
        DB.child('comments/' + key + '/participants').update({
            uid: True
        })
        classify_text(decoded_json['description'], key)
        user_name = request.user.name
        user_picture = request.user.user_picture
        if decoded_json['local_assistance']:
            notify_incident(sender_uid=uid, datetime=int(time.time()*1000),
                            event_id=key, event_type=decoded_json['category'],
                            lat=latitude, lng=longitude,
                            user_text=decoded_json['title'],
                            user_name=user_name, user_picture=user_picture)

        classify_text(decoded_json['description'], key)
        return JsonResponse({"eventId":str(key)}) 

class MultipleEventsView(APIView):
    """API View for grouping incidents by location
    """

    def get(self, request):
        """Returns events within a certain radius for a given location

        POST request parameters:
            [REQUIRED]
            lat: latitude of the location

            lng: longitude of the location

            dist: maximum radius of the location

        Arguments:
            request {[type]} -- [ Contains the django request object]

        Returns:
            [HttpResponseBadRequest] -- [If  any of the required parameters is
                                        not given.]
            [JsonResponse] -- [Containing the event data]
        """

        # Should use API View here
        lat = float(request.GET.get('lat', ''))
        lng = float(request.GET.get('lng', ''))
        ghash = long(request.GET.get('ghash',''))
        thresold = float(request.GET.get('dist', ''))
        if lat == '' or lng == '' or ghash == '' or thresold == '':
            return HttpResponseBadRequest("Bad request")

        incidents = DB.child('incidents').get()
        data = []

        for incident in incidents.each():
            event = dict(incident.val())
            temp = {}
            temp['key'] = incident.key()
            temp['lat'] = event['location']['coords']['latitude']
            temp['lng'] = event['location']['coords']['longitude']
            temp['ghash'] = event['location']['geohash']
            temp['category'] = event['category']
            temp['title'] = event['title']
            temp['datetime'] = event['datetime']
            tmplat = float(event['location']['coords']['latitude'])
            tmplng = float(event['location']['coords']['longitude'])
            tmpghash = long(event['location']['geohash'])
            dist = geohash_approximate_distance(tempghash, ghash)
            if dist < thresold:
                data.append(temp)

        # Cluster the events
        cluster_thresold = float(request.GET.get('min', 0))
        # This code should also be present on client side
        if cluster_thresold:
            # clustered incidents data
            clustered_data = []
            # Consider each node as root for now
            for root in data:
                # If is clustered flag is not present
                if not root.get('isClustered', False):
                    # Loop though the points
                    for child in data:
                        # Base case
                        if child['key'] == root['key']:
                            continue
                        # If node is not clustered
                        if not child.get('isClustered', False):
                            # Calculate the distance
                            temp_distance = geohash_approximate_distance(root['ghash'],
                                                     child['ghash'])
                            # If two points are too close on map cluster them
                            if temp_distance < cluster_thresold:
                                # Update root
                                root['isClustered'] = True
                                root['lat'] = (root['lat'] + child['lat'])/2
                                root['lng'] = (root['lng'] + child['lng'])/2
                                # Mark child
                                child['isClustered'] = True
                    clustered_data.append(root)
            return JsonResponse(clustered_data, safe=False)
        return JsonResponse(data, safe=False)
