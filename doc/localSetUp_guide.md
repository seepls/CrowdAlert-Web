### Git set up on your local machine 

#### 1. fork 
```
git clone https://gitlab.com/name/CrowdAlert-Web.git
```
#### 2. add upstream and pull
```
git remote add upstream https://gitlab.com/aossie/CrowdAlert-Web.git
git remote -v
git pull upstream master 
```
#### 3. virtual environment 
```
virtualenv env --python= python3 
. env/bin/activate
which python 
```
#### 4. install packages in root  
```
pip install -r requirements.txt
```
