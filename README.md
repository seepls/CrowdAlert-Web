# CrowdAlert Web

Crowdalert is a Progressive Web Application for reporting and viewing incidents across the globe.


#### Screenshots

<table>
    <tr>
     <td><kbd><img src="./doc/Screenshots/signup.png"></kbd></td>
     <td><kbd><img src="./doc/Screenshots/alert.png"></kbd></td>
     <td><kbd><img src="./doc/Screenshots/map.png"></kbd></td>
     <td><kbd><img src="./doc/Screenshots/report.png"></kbd></td>
     <tr> 
      <td><kbd><img src="./doc/Screenshots/incident.png"></kbd></td>
      <td><kbd><img src="./doc/Screenshots/dashboard1.png"></kbd></td>
      <td><kbd><img src="./doc/Screenshots/report1.png"></kbd></td>
    </tr>
  </table>

----


## Local Set up

##### basic installations 
Follow the  [Basic guide](doc/basic_guide.md) to set up basic dependencies for the project .
Follow the [Local guide](doc/localSetUp_guide.md) to set up the project locally .
## API keys

Run this in the project root directory

```bash
echo "\
REACT_APP_GOOGLE_MAPS_KEY=\

REACT_APP_FACEBOOK_APP_ID=\

REACT_APP_FIREBASE_API_KEY=\

REACT_APP_FIREBASE_AUTH_DOMAIN=\

REACT_APP_FIREBASE_DATABASE_URL=\

REACT_APP_FIREBASE_PROJECT_ID=\

REACT_APP_FIREBASE_SENDER_ID=\
" > .env.local
```

Update the file with your own keys

## Preview

`npm start`

## Deploying

```bash
npm run build
gunicorn -b 0.0.0.0:8000 CrowdAlert.wsgi
```
